## Bash bang

- running the command `bash` would open a new bash.
- `man bash` will give the bash documentation. (Bash manual)
- `info bash` bash info interface, we can search here.
- [Bash Manual](https://www.gnu.org/software/bash/manual/bash.html)
- First 2 characters should be #! (shebang) followed by the path name to bash program usually `#!/usr/bin/env bash` or `#!/bin/bash` 
- `ps -l` lists all process


- **The Bash Time Command:** bash has a built in time command
- we can use the time command with some other command which will give how long the other command took. ex - `time find / -name core`
- `unset` bash command can be used to remove the variable.
- Reference the value of a variable with a $ sign in front of the name `echo myvar is $myvar`
- Shell keeps variable in two different areas. The area called **environment or exported variables** are copied across the many shell or into new shell. We can export an variable. `export var2="This is a value"` now bash exports **var2** to exported variables. New shell/ program requires copy of this variable not shared.
- running just `export` will give exported variables.
-  Get a list of Bash builtins with the `enable` command
- `compgen -k` will list out all the keywords in bash.
- .bash_profile is read when Bash is invoked as a login shell.
- .bashrc is executed when a new shell is started.
- setting path in bash startup ex - `PATH=$PATH:/usr/local/bin` this would keep adding /usr/local/bin to the end of the PATH. $PATH is current value of path, so it is concatenating with PATH
- `echo` command is used to print, it's built into Bash and doesn't start new process. few options are
	`-n` -> don't print a training new line
	`-e` -> enable backslash escaped characters like \n and \t
	`-E` -> disable backslash
- `echo *` will show file and directory names
- We can also use redirection technique with echo, following will send the error message to standard error (&2 is stderr)
`echo "Warning line!" >&2`

- Local variables and Typeset help to run the script faster. It's always good practice to use less global variables. The typeset command makes variable local, can provide a type or can provide formatting.
`typeset -i x` here x must be an integer.
- The Declare command helps to declare a variable with some features. 
	- `declare -l` converts upper case to lower case
	- `declare -u` converts to upper care.
	- `declare -r` variable is made read only.
	- `declare -a MyArray` will make MyArray an indexed array.
	- `declare -A MyArray` will make an associative array or dict or hash
- `read` command helps to read input 
- if we want to have more than one statement in a line use semi colon `;` to separate the statements.
- **The Exit Command:** `exit <VALUE>` sets the exit status. Exit terminates the shell process. 0 means success. 
- `$?` gives exit status of a program.
- **Redirection and Pipes:** Process normally have three files open 0 => stdin, 1 => stdout, 2 => stderr
- Synatx - `command > stdout-here 2> stderr-here < stdin-from-here`
- here, `<` redirect from the standard input and `>` is writing out to standard out. 
- `command | command2
#Here command2's stdin comes from # command's stdout` so output of command will be the input to command2.
- `command >> file` appends to the end of the file.
- `exec N< myfile` Opens a file descriptor N for reading from file *myfile*
- `exec N> myfile` Opens a file descriptor N for writing to *myfile*
- `exec N<> myfile` Opens file file descriptor N for reading and writing to *myfile*
- `losf` - List Open Files `-p` will give process id, $$  will say file descriptor.
- The builtin **test** is used to check various conditions and set the return code with the result.
- An alternate to test is `[[  ]]` or `((  ))`
- `if test -f afile` or `if [[ -f file ]]`  here -f check if it ia a file or not. -d check's if a variable is directory or not and -s if exists or not.
- **Arithmetic Operators:** use in `((  ))` or ***with let***
- **Filters: **In Linux, a program is a "filter" if it reads from stdin and writes to stdout.
- Filters can ve used in piles. 
- ***head*** prints the first n lines of a file or stdin
- ***tail*** prints the last n lines of a file of stdin.
	- `ls -l | head -5` first 5 lines of ls - l
	- `ls -l | head -10 | tail -5` lines 6-10 head -10 gives the first 10 lines and tail -5 gives last 5 lines of first 10.
- ***wc*** (word count) prints line, word and char counts
	- `wc -l` prints the number of lines
- **The AWK Language:** A pattern matching language, works as filter, good for syntax writing.
	- Fields are delimited by the values in the variable FS normally white space.
	- $0 means entire line.
- **Positional Parameters:** We can use $0, $1, $2 . . .  $ {10} and so on to represent the positional parameters.
	`${variable <OPR> value};x=${var:-defaultValue}`
	- `:-` if var is unset/null, return default value otherwise value in the var. like getOrDefault
	- `:=` this assigns the value if var is unset/null and also return back
	- `:?` displays an error and exit script if var is unset / null
	- `:+` this will give nothing if value is unset/null otherwise the value.
	- `${var:offset}` value of var starting at offset specially for Strings, also the extension version `${var:offset:len}`
	- `${#var}` length of var, how many character
	- `${var#prefix}` removes matching prefix from beginning of var
	- `${var#postfix}` removes matching from end of the var.