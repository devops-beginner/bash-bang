#!/bin/bash

# This script demonstrate the use of sleep fucntion in bash
# The script gonna sleep for number of seconds passed as parameter

DELAY=$1

# if Delay has no value or blank

if [ -z $DELAY ]; then
    echo "You must pass a delay"
    exit 1
fi

echo "Going to sleep for $DELAY seconds"
sleep $DELAY
echo "UP!"
exit 0

# We can make this script run on background during the delay.
# by adding  "&" ex - bash sleep-demo.sh 10 &
