#!/bin/bash

# This is a script to watch a process and show up a message ater the process orver
status=0

if [ -z $1 ]; then
    echo "Required a process id"
    exit 1
fi

# now have to change the status to change
while [ $status -eq 0 ]; do
    ps $1 >/dev/null # ps will list all the process and redirecting it to null
    status=$?
done

echo "Process $1 terminated"

# ps -a is command to list all process
# running sleep and watch scripts together.
# start the sleep script with background operator
# bash sleep-demo.sh 30 &
# then get the process id and run the watch process script
