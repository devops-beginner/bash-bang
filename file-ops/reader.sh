#!/bin/bash

COUNT=1

# IFS-internal filed separator
# -r -> don't allow back slash
# LINE is the variable to hold current line of text
while IFS='' read -r line; do
    echo "Line $COUNT: $line" # displays line number and line
    ((count++))
done <"$1" # $1 is first argument that is file. less than is to use redirection so we can use passed file as input.

# function to get owner of all files in this directory
function getOwners() {
    ls -l | while
        read a b c d
    do
        echo owner is $c
    done
}

getOwners

exit 0

# redirections
# running 'bash reader.sh sample.txt > output.txt' -> this creates a new output file if doesn't exists and writes the content, if there an output file, then it will erase the content and write.
# running 'bash reader.sh sample.txt >> output.txt' -> this will not erase the content but it appends new content.
