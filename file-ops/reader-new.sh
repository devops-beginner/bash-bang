#!/bin/bash

# counter to track line number
count=1

while IFS='' read -r line; do
    echo "Line #$count - $line"
    if [ $count -ge 3 ]; then
        break
    fi
    ((count++))
done <"$1"

exit 0
