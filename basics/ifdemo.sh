#!/bin/bash

#syntax for if statement is - 

# if [ condition]
# then
#     [statements]
# fi

# fi has no special meaning, it's just end of the if.

color=$1
if [ $color = "blue" ]
then
    echo "The color is Blue"
fi

# Notice here using = for comparision instead of ==, double
# -eq -> first arg equal to second
# -ne -> not equal
# -lt -> less than or equals
# -gt -> greater than 
# -le -> less than or equals 
# -ge -> greater than or equals

name_one=$2
age_one=$3
name_two=$4
age_two=$5

if [ $age_one -gt $age_two ]; then
     # body
     echo $name_one is elder than $name_two
fi

# if else
if [ $age_one -gt $age_two ]; then
     # body
     echo $name_one is elder than $name_two
elif [ $age_one -lt $age_two ]; then
     # body
     echo $name_two is elder than $name_one
else
     # body
     echo $name_one and $name_two are same age!
fi
