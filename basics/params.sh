#!/bin/bash
first_name=$1
last_name=$2

echo "Welcome ${first_name} ${last_name} to the params!"
echo "Time - "$(date)
echo "Current working directory - "$(pwd)

# here notice, the commands like date, pwd are enclosed within the braces, because mentioning $pwd looks for an variable which is not defined.
