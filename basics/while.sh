#!/bin/bash

# Counter which holds count
count=0

# Syntax for while loop
# while [ condition ]; do
#     # body
# done

while [ $count -lt 10 ]; do
    echo "Count = $count"
    ((count++))
done

# Another syntax of while is

# while
#     command list
# do
#     command list
# done
# loops while command list 1 succeeds
function getOwners() { # piping ls with while.
    ls -l | while
        read a b c d
    do
        echo owner is $c
    done
}

getOwners

echo "While loop completes, exit"

exit 0
