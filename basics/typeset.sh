#!/bin/bash

# This script demonstartes typeset

function fun() {
    typeset x
    x=7
    y=9
}

function fun2() {
    local x
    typeset -i m=3
    x=7
    y=9

    echo "Value of m before change $m"
    m="a"
    echo "Value of m after change $m"
}

x=2
y=8

echo x is $x
echo y is $y

#call function
fun
fun2

echo "After function call"

echo x is $x
echo y is $y

# Here in the function fun, using the typeset to indicate x is local. Hence the value of x don't change even after changing x value in function. We can also # use local keyword for this, notice fun2.
# Also, notice in fun2, using -i makes sure that m accepts only integer, tyring to assign anyother value will change it's value to 0.
