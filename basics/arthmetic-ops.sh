#!/bin/bash

# this script demonstrates some operations

function fun1() {
    n=5
    ((n++))
    if ((n < 4 || n == 0)); then
        echo "n is leass than 4 or 0. $n"
    else
        echo "n is $n"
    fi
}

function fun2() {
    ((m = 2 ** 3 + 5))
    ((y = m ^ 4)) # XOR
    echo y = $y
}

fun1
fun2
