#!/bin/bash

# This script to demonstarte echo command

# echo is used to print the message on console.
echo "Hello World!"

echo -e "Hello \nWorld" # new line
