#!/bin/bash

# This is the script to demonstare break, continue and how to read more than one args.

# $@ holds the all the parameters user enters into an array then we can access it
names=$@

# syntax for looping the collection
# for item in {a..z}; do
#     echo "$item"
# done

for name in $names; do
    if [ $name = "venky" ]; then
        break
    elif [ $name = "kiran" ]; then
        continue
    fi
    echo "Hello $name"
done

echo "For loop ends"
exit 0
