#!/bin/bash

# This script to learn the usage of declare commamd.

declare -l lString="THIS WAS A UPPER STRING"
declare -u uString="this was a lower string"
declare -r readOnlyString="This is read only string"

declare -a myArray      # declaring index based array
declare -A myAssocArray # declaring associative array or dict or map

echo lString = $lString
