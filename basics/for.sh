#!/bin/bash

# $@ holds the all the parameters user enters into an array then we can access it
names=$@

# syntax for looping the collection
# for item in {a..z}; do
#     echo "$item"
# done

for name in $names; do
    echo "Hello $name"
done

# The `seq 1 5` or $(seq 1 5) generates the number from 1-5 and we can loop through it.
for num in $(seq 1 5); do
    echo $num
done

# Generate sequences with {A..Z}, {1..10}
for alpha in {a..m}; do
    echo $alpha
done

for digit in {3..45}; do
    echo $alpha
done

# displays every word, it is similar to cat the data file. so whole content is loaded into this for loop
for d in $(<"data-file.txt"); do
    echo $d
done

# Displays all the files that have .sh extension
for fileName in *.sh; do
    echo file name - $fileName
done

exit 0
