#!/bin/bash

# upper limit of count
upper=$1

# Syntaf for loop using index
# for((i=0;i<n;i++)); do
#     echo "$i"
# done

for ((i = 0; i < $upper; i++)); do
    echo "Counter value - $i"
done
