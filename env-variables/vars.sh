#!/bin/bash

# Printing the path
echo "The path is $PATH"

# Printing the terminal
echo "The terminal is $TERM"

# if the editor is set in our config it will display otherwise a message
# note -z is used to check if a variable is blank
if [ -z $EDITOR]; then
    echo "The Editor is not set"
else
    echo "The Editor is $EDITOR"
fi

# If else syntax
# if [ condition ]; then
#      # body
# elif [ condition ]; then
#      # body
# else
#      # body
# fi

# Some of the environment variables are
# HOME - user's home directory
# PATH - directories which are searched for command
# HOSTNAME - hoastname of the machine
# SHELL - shell that is being used
# USER - user of this session
# TERM - Type of command line termonal that is being used.
