#!/bin/bash

# Set a global variable Computer to a number between 1 to 50
# take input from the user. if the user's input matches computer then they won.

computer=16
steps=1

read -p "Welcome to the Guess Game! Please enter your name: " name
echo "-------------------------------------------------------------------"
echo "Rules: The computer guessed a number between 1 and 50."
echo "You guess the number till you get there!"

userIp=0

while [ $userIp -ne $computer ]; do
    read -p "Enter your guess: " userIp

    if [ $userIp -eq $computer ]; then
        break
    elif [ $userIp -lt $computer ]; then
        echo "Too low..!"
        ((steps++))
        continue
    elif [ $userIp -gt $computer ]; then
        echo "Too high..!"
        ((steps++))
        continue
    fi

done

echo "$name you won in $steps"
exit 0
