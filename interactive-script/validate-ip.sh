#!/bin/bash

# This scripts accepts some information and validates it

isValid=0

while [ $isValid -eq 0 ]; do
    read -p "Enter your name and age:" name age
    # check any of two data are empty
    if [[ (-z name) || (-z age) ]]; then
        echo "Not enough parameters, give <name> <age>"
        continue
    elif [[ ! $name =~ ^[A-Za-z]+$ ]]; then # checking if name has any non alpha.
        echo "Non alpha characters in $name"
        continue
    elif [[ ! $age =~ ^[0-9]+$ ]]; then
        echo "Invalid age, age should be a number"
        continue
    fi
    isValid=1
done

echo "User name - $name and age - $age"
exit 0
