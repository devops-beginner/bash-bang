- To find out what your default interpreter is, type this in the command window
    > `ps $$`

- `which shell` or `echo $SHELL` find out the full execution path of your shell interpreter.
- `whoami` command shows who is the user.
- Some of the keyboard shortcuts for command line utility.
    - Esc + T: Swap the last two words before the cursor
    - Ctrl + H: Delete the letter starting at the cursor
    - Ctrl + W: Delete the word starting at the cursor
    - TAB: Auto-complete files, directory, command names and much more
    - Ctrl + R: To see the command history.
    - Ctrl + U: Clear the line
    - Ctrl + C: Cancel currently running commands.
    - Ctrl + L: Clear the screen
    - Ctrl + T: Swap the last two characters before the cursor

- Some of the Special Characters in Bash
![](https://gitlab.com/hegdevenky/static-files/-/raw/master/devops/other/special-chars.jpg)
