#!/bin/bash

# There are two ways to create a function using word "function function_name(){}"
# Other way is similar but we can leave the word "function "

function helloWorld() {
    local name=$1               # assigning the first incoming parameter to a local variable name. We can also assign global variable
    echo "Hello World $name $2" # diretly printing the second parameter
}

# Another function

goodBye() {
    echo "Goodbye $1!" # here, instead of assigning, directly printing the first param.
}

echo "*****************************************************"
# calling functions
helloWorld Bob Vence # notice here passing the parameter
goodBye Bob          # notice here passing the parameter
echo "*****************************************************"
echo "Function ends"

# Notice here that, we don't use "()" while calling the functions
# Function must be defined before they calling, other wise we will get "<function_name> Command not found" error.
