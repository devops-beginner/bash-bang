#!/bin/bash
# There are two ways to create a function using word "function function_name(){}"
# Other way is similar but we can leave the word "function "

function helloWorld() {
    echo "Hello World"
}

# Another function

goodBye() {
    echo "Goodbye!"
}
echo "*****************************************************"
# calling functions
helloWorld
goodBye
echo "*****************************************************"
echo "Function ends"

# Notice here that, we don't use "()" while calling the functions
# Function must be defined before they calling, other wise we will get "<function_name> Command not found" error.
