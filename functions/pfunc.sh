#!/bin/bash

# Gets the first 10 files frim this directory in sorted order
getFiles() {
    FILES=$(ls -1 | sort | head -10) # here files acts as global variable
}

# Shows the files
showFiles() {
    local count=1
    for file in $FILES; do
        echo "File #$count - $file"
        ((count++))
    done
}

showFilesAnother() {
    local count=1
    for file in $@; do # accepting collection of files as parameter
        echo "File #$count - $file"
        ((count++))
    done
}

getFiles
showFiles
echo "------------------------------------"
getFiles
showFiles $FILES # passing the global variable as parameter show files.

echo "end of the script"
exit 0
