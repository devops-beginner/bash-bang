#!/bin/bash

# displaying first trhee files from directory in reverse alphabetical order and each file will have a count as well
files=$(ls -1 | sort -r | head -3)
# that can also be written as - files=`ls -1 | sort -r | head -3`
# here ls -1 list's the first colum of ls result, sort is linux sort method -r indicates the reverse order, head -3 says take the first thee results

count=1

for file in $files; do
    echo "File #$count = $file"
    ((count++))
done

exit 0
