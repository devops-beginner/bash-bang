#!/bin/bash

# This script demonstates the fucntions that return some values

# function addTwoNumbers() {

# }

function myfunc() {
    echo starting myfunc
    return
    echo this will not be executed
}

# Functions produce results by writing output like commands do
function myfunc2() {
    echo starting
    return
    echo this will not be executed
}

myfunc
n=$(myfunc)
echo n is $n

result=$(myfunc2)
echo $result

exit 0
